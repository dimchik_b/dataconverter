# Data Converter

This project is a test task for employment

There is a data source that feeds us with the fixed point decimal data. Our code shall convert it to the float point data.
The input data is organized in packages with a header and a data block as

	struct InputBlock
	{
		short int freq_khz;
		short int samplerate_ksmpls;
		short int data[2048];
	};
We can obtain these data by calling the function *ReceiveData* with a signature

    int (void *ptr, size_t size);
There could be many such functions that represent different data channels. These functions are not blocking and return immediately as many 
bytes as they have ready. If some error happens, they return a negative number. In the context of this task, we should ignore errors.

We shall return the converted data calling the function 

	void PostProcess(int ch, float *block, int size, float freq_hz,
					 int samplrate_smpls);
where

	ch - the channel where the data are received
	block - a pointer to the data block
	size - a sample number in the block. Should be 5120
	freq_hz - a frequency of the channel in Hz
	samplerate_smpls - a sample rate of the channel in samples per second

The class *Channel* shall represent an individual channel that has a number and a handler. The handler is a pointer to the specific *ReceiveData* function.
We shall supply the *Channel* class with the channel number and the handler at
 the time of creation. This class shall have methods *StartCH()* and *StopCH()* 
which run and stop the channel processing, respectively. Both the methods 
shall be asynchronous.

Different channels, up to 4, should be organized into the class *ConversionFacade* with methods 

    void AddCH(int channelNo, ReceiveHandler h);
    void RemoveCH(int channelNo);
    void StartCH(int channelNo);
    void StopCH(int channelNo);
    std::unique_ptr<Channel> GetCH(int channelNo);
The last method shall return a pointer to the *Channel* object to operate it independently of the facade object.
