#include "DataConverter.h"
#include "gmock/gmock.h"
#include <memory>

using testing::_;

void createInputBlock(int16_t *input)
{
    for (int i = 0; i < INPUT_BLOCK_SIZE / 8; i++)
    {
        input[8 * i + 0] = 0b0'00'0000000000000; // 0.0
        input[8 * i + 1] = 0b1'00'0000000000000; // 0.0
        input[8 * i + 2] = 0b0'01'0000000000000; // 1.0
        input[8 * i + 3] = 0b1'01'0000000000000; // -1.0
        input[8 * i + 4] = 0b1'01'1000000000000; // -1.5
        input[8 * i + 5] = 0b0'01'1000000000000; // 1.5
        input[8 * i + 6] = 0b0'11'0110000000000; // 3.375
        input[8 * i + 7] = 0b1'10'0010000000000; // -2.125
    }
}

TEST(DataConverter, convertBlock_makes_output_block)
{
    DataConverter dataConverter;
    int16_t input[INPUT_BLOCK_SIZE];
    createInputBlock(input);

    const int OUT_BLOCKS = 5;
    OutputBlock *output[OUT_BLOCKS] = {nullptr};

    int res = dataConverter.convertBlock(input, INPUT_BLOCK_SIZE, output, 5);
    EXPECT_EQ(res, 0);
    res = dataConverter.convertBlock(input, INPUT_BLOCK_SIZE, output, 5);
    EXPECT_EQ(res, 0);
    res = dataConverter.convertBlock(input, INPUT_BLOCK_SIZE, output, 5);
    EXPECT_EQ(res, 1);

    for (int i = 0; i < OUTPUT_BLOCK_SIZE / 8; i++)
    {
        EXPECT_EQ(output[0]->data[8 * i + 0], +0.000) << "i = " << 8 * i;
        EXPECT_EQ(output[0]->data[8 * i + 1], +0.000) << "i = " << 8 * i + 1;
        EXPECT_EQ(output[0]->data[8 * i + 2], +1.000) << "i = " << 8 * i + 2;
        EXPECT_EQ(output[0]->data[8 * i + 3], -1.000) << "i = " << 8 * i + 3;
        EXPECT_EQ(output[0]->data[8 * i + 4], -1.500) << "i = " << 8 * i + 4;
        EXPECT_EQ(output[0]->data[8 * i + 5], +1.500) << "i = " << 8 * i + 5;
        EXPECT_EQ(output[0]->data[8 * i + 6], +3.375) << "i = " << 8 * i + 6;
        EXPECT_EQ(output[0]->data[8 * i + 7], -2.125) << "i = " << 8 * i + 7;
    }

    for (int i = 0; i < OUT_BLOCKS; i++)
    {
        delete output[i];
        output[i] = nullptr;
    }
}

TEST(DataConverter, abort)
{
    DataConverter dataConverter;
    int16_t input[INPUT_BLOCK_SIZE];
    createInputBlock(input);

    const int OUT_BLOCKS = 5;
    OutputBlock *output[OUT_BLOCKS] = {nullptr};

    int res = dataConverter.convertBlock(input, INPUT_BLOCK_SIZE, output, 5);
    EXPECT_EQ(res, 0);
    res = dataConverter.convertBlock(input, INPUT_BLOCK_SIZE, output, 5);
    EXPECT_EQ(res, 0);
    dataConverter.abort();
    res = dataConverter.convertBlock(input, INPUT_BLOCK_SIZE, output, 5);
    EXPECT_EQ(res, 0);

    for (int i = 0; i < OUT_BLOCKS; i++)
    {
        delete output[i];
        output[i] = nullptr;
    }
}
