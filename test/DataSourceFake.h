#ifndef DATA_SOURCE_FAKE_H
#define DATA_SOURCE_FAKE_H

#include <stdint.h>

void addData(void *data, int size);
int receiveData(void *ptr, size_t size);
int getRemainAndReset();

#endif // DATA_SOURCE_FAKE_H
