#include "ConversionFacade.h"
#include "DataSourceFake.h"
#include "PostProcessMock.h"
#include <gmock/gmock.h>

using testing::_;

const int CHANNEL_NO = 123;

TEST(ConversionFacade, AddCH)
{
    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    ConversionFacade facade;

    facade.AddCH(CHANNEL_NO, &receiveData);
    facade.StartCH(CHANNEL_NO);

    InputBlock inp[3] = {0};
    addData(reinterpret_cast<char *>(inp), sizeof(inp));
    std::this_thread::sleep_for(std::chrono::milliseconds(30));

    facade.StopCH(CHANNEL_NO);

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(ConversionFacade, AddCH_without_start)
{
    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(0);

    ConversionFacade facade;
    facade.AddCH(CHANNEL_NO, &receiveData);
    // no facade.StartCH(CHANNEL_NO);

    InputBlock inp[3] = {0};
    addData(reinterpret_cast<char *>(inp), sizeof(inp));
    std::this_thread::sleep_for(std::chrono::milliseconds(30));

    facade.StopCH(CHANNEL_NO);

    ASSERT_EQ(getRemainAndReset(), sizeof(inp));
}

TEST(ConversionFacade, StopCH)
{
    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(0);

    ConversionFacade facade;
    facade.AddCH(CHANNEL_NO, &receiveData);
    facade.StartCH(CHANNEL_NO);
    facade.StopCH(CHANNEL_NO);

    InputBlock inp[3] = {0};
    addData(reinterpret_cast<char *>(inp), sizeof(inp));
    std::this_thread::sleep_for(std::chrono::milliseconds(30));

    ASSERT_EQ(getRemainAndReset(), sizeof(inp));
}

TEST(ConversionFacade, RemoveCH)
{
    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(0);

    ConversionFacade facade;

    facade.AddCH(CHANNEL_NO, &receiveData);
    facade.RemoveCH(CHANNEL_NO);
    facade.StartCH(CHANNEL_NO);

    InputBlock inp[3] = {0};
    addData(reinterpret_cast<char *>(inp), sizeof(inp));
    std::this_thread::sleep_for(std::chrono::milliseconds(30));

    facade.StopCH(CHANNEL_NO);

    ASSERT_EQ(getRemainAndReset(), sizeof(inp));
}

TEST(ConversionFacade, GetCH)
{
    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    ConversionFacade facade;

    facade.AddCH(CHANNEL_NO, &receiveData);
    auto channel = facade.GetCH(CHANNEL_NO);
    channel->StartCH();

    InputBlock inp[3] = {0};
    addData(reinterpret_cast<char *>(inp), sizeof(inp));
    std::this_thread::sleep_for(std::chrono::milliseconds(30));

    channel->StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}
