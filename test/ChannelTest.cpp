#include "Channel.h"
#include "DataSourceFake.h"
#include "PostProcess.h"
#include "PostProcessMock.h"
#include "gmock/gmock.h"
#include <algorithm>
#include <thread>

using std::shared_ptr, std::make_shared;
using testing::_;
using testing::Invoke;

const int CHANNEL_NO = 123;
const int ODD_NUMBER = 789;

TEST(Channel, process_0_bytes)
{
    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, _, 0, 0)).Times(0);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_error)
{
    const int ERROR_CODE = -1;

    InputBlock inp[3] = {0};
    addData(inp, sizeof(inp[0]));
    addData(inp + 1, sizeof(inp[1]));
    addData(nullptr, ERROR_CODE);
    addData(inp + 2, sizeof(inp[2]));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_1_byte)
{
    InputBlock inp[3] = {0};
    char *firstBlock = reinterpret_cast<char *>(inp);
    addData(firstBlock, 1);
    addData(firstBlock + 1, sizeof(inp[0]) - 1);
    addData(inp + 1, sizeof(inp[1]));
    addData(inp + 2, sizeof(inp[2]));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_only_freq)
{
    InputBlock inp[3] = {0};
    char *firstBlock = reinterpret_cast<char *>(inp);
    const size_t SIZE_OF_FREQ = sizeof(inp[0].freq_khz);
    addData(firstBlock, SIZE_OF_FREQ);
    addData(firstBlock + SIZE_OF_FREQ, sizeof(inp[0]) - SIZE_OF_FREQ);
    addData(inp + 1, sizeof(inp[1]));
    addData(inp + 2, sizeof(inp[2]));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_only_header)
{
    InputBlock inp[3] = {0};
    char *firstBlock = reinterpret_cast<char *>(inp);
    const size_t SIZE_OF_HEADER = reinterpret_cast<char *>(&inp[0].data[0]) - firstBlock;
    addData(firstBlock, SIZE_OF_HEADER);
    addData(firstBlock + SIZE_OF_HEADER, sizeof(inp[0]) - SIZE_OF_HEADER);
    addData(inp + 1, sizeof(inp[1]));
    addData(inp + 2, sizeof(inp[2]));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_header_plus_1_byte)
{
    InputBlock inp[3] = {0};
    char *firstBlock = reinterpret_cast<char *>(inp);
    const size_t SIZE_OF_HEADER_1 = reinterpret_cast<char *>(&inp[0].data[0]) - firstBlock + 1;
    addData(firstBlock, SIZE_OF_HEADER_1);
    addData(firstBlock + SIZE_OF_HEADER_1, sizeof(inp[0]) - SIZE_OF_HEADER_1);
    addData(inp + 1, sizeof(inp[1]));
    addData(inp + 2, sizeof(inp[2]));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_full_output_block)
{
    InputBlock inp[3] = {0};
    addData(inp, sizeof(inp));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_two_output_blocks)
{
    InputBlock inp[5] = {0};
    addData(inp, sizeof(inp));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 0)).Times(2);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_freq_changed)
{
    InputBlock inp[5] = {0};
    addData(inp, sizeof(inp));
    for (int i = 1; i < 5; ++i)
    {
        inp[i].freq_khz = 100;
    }

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 100000, 0)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, process_samplerate_changed)
{
    InputBlock inp[5] = {0};
    addData(inp, sizeof(inp));
    for (int i = 1; i < 5; ++i)
    {
        inp[i].samplerate_ksmpls = 100;
    }

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, OUTPUT_BLOCK_SIZE, 0, 100000)).Times(1);

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}

TEST(Channel, oddNumberOfBytes)
{
    InputBlock inp[3] = {};
    for (auto &block : inp)
    {
        for (int i = 0; i < INPUT_BLOCK_SIZE; ++i)
        {
            block.data[i] = (i % 4) << FIXED_POINT_POWER;
        }
    }
    char *firstBlock = reinterpret_cast<char *>(inp);
    const size_t SIZE_OF_HEADER_PLUS_ODD = reinterpret_cast<char *>(&inp[0].data[0]) - firstBlock + ODD_NUMBER;
    addData(firstBlock, SIZE_OF_HEADER_PLUS_ODD);
    addData(firstBlock + SIZE_OF_HEADER_PLUS_ODD, sizeof(inp[0]) - SIZE_OF_HEADER_PLUS_ODD);
    addData(inp + 1, sizeof(inp[1]));
    addData(inp + 2, sizeof(inp[2]));

    auto postProcessMock = getPostProcessMock();
    EXPECT_CALL(*postProcessMock, PostProcess(CHANNEL_NO, _, _, 0, 0))
        .WillOnce(Invoke([](int channel, float *data, int size, float freq, int samplerate)
                         {  ASSERT_EQ(size, OUTPUT_BLOCK_SIZE);
                            for (int i = 0; i < size; ++i)
                            {
                                if(data[i] != i % 4)
                                {
                                    FAIL() << "data[" << i << "] = " << data[i] << " != " << i % 4;
                                }
                            } }));

    Channel channel(&receiveData, CHANNEL_NO);
    channel.StartCH();
    std::this_thread::sleep_for(std::chrono::milliseconds(30));
    channel.StopCH();

    ASSERT_EQ(getRemainAndReset(), 0);
}
