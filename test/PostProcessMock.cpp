#include "PostProcessMock.h"

static std::weak_ptr<PostProcessMock> sPostProcessMock;

std::shared_ptr<PostProcessMock> getPostProcessMock()
{
    auto ptr = sPostProcessMock.lock();
    if (!ptr)
    {
        ptr = std::make_shared<PostProcessMock>();
        sPostProcessMock = ptr;
    }
    return ptr;
}

void PostProcess(int ch, float *block, int size, float freq_hz,
                 int samplrate_smpls)
{
    if (sPostProcessMock.use_count())
    {
        sPostProcessMock.lock()->PostProcess(ch, block, size, freq_hz,
                                             samplrate_smpls);
    }

    delete block;
    block = nullptr;
}
