#ifndef POST_PROCESS_MOCK_H
#define POST_PROCESS_MOCK_H

#include <gmock/gmock.h>
#include <memory>

struct PostProcessMock
{
    MOCK_METHOD5(PostProcess, void(int, float *, int, float, int));
};

std::shared_ptr<PostProcessMock> getPostProcessMock();

#endif // POST_PROCESS_MOCK_H