#include "DataSourceFake.h"
#include <string.h>
#include <vector>

struct SampleData
{
    struct DataBlock
    {
        char *data;
        int size;
    };

    void addData(void *data, int size)
    {
        mDataBlocks.push_back({reinterpret_cast<char *>(data), size});
    }

    int getData(char *data, int size)
    {
        int sendSize = 0;
        if (data && mDataBlocks.size())
        {
            auto &block = mDataBlocks.front();
            sendSize = std::min(size, block.size);
            if (sendSize > 0)
            {
                memcpy(data, block.data, sendSize);
            }
            block.data += sendSize;
            block.size -= sendSize;
            if (block.size == 0)
            {
                mDataBlocks.erase(mDataBlocks.begin());
            }
        }
        return sendSize;
    }

    void reset()
    {
        mDataBlocks.clear();
    }

    int getRemain()
    {
        int res = 0;
        for (auto &block : mDataBlocks)
        {
            res += block.size;
        }
        return res;
    }

private:
    std::vector<DataBlock> mDataBlocks;
};

static SampleData sSampleData;

int receiveData(void *ptr, size_t size)
{
    return sSampleData.getData(static_cast<char *>(ptr), size);
}

void addData(void *data, int size)
{
    sSampleData.addData(data, size);
}

int getRemainAndReset()
{
    auto res = sSampleData.getRemain();
    sSampleData.reset();
    return res;
}
