#include "Channel.h"
#include "PostProcess.h"
#include <thread>

Channel::Channel(ReceiveHandler handler, int channelId)
    : mHandler(handler), mChannelId(channelId)
{
}

Channel::~Channel()
{
    StopCH();
}

void Channel::StartCH()
{
    mActive = true;
    if (!mRunThread.joinable())
    {
        mRunThread = std::thread(&Channel::run, this);
    }
}

void Channel::StopCH()
{
    mActive = false;
    if (mRunThread.joinable())
    {
        mRunThread.join();
    }
}

void Channel::run()
{
    const int HEADER_SIZE = sizeof(InputBlock::freq_khz) + sizeof(InputBlock::samplerate_ksmpls);
    const size_t OUT_BLOCKS = 1;

    float freq_hz{};
    int samplerate_smpls{};

    {
        std::lock_guard<std::mutex> lock(mMutex);
        freq_hz = mFreq_hz;
        samplerate_smpls = mSamplrate_smpls;
    }

    while (mActive.load() && mHandler)
    {
        InputBlock inp;
        char *readPos = reinterpret_cast<char *>(&inp);
        int16_t *processPos = nullptr;
        const char *BUFF_END = readPos + sizeof(inp);
        bool checkParameters = true;
        OutputBlock *outPtrs[OUT_BLOCKS] = {nullptr};

        while (readPos < BUFF_END && mActive.load())
        {
            int bytesRead = mHandler(readPos, BUFF_END - readPos);

            if (bytesRead <= 0) // error or no data
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                continue; // no error proceccing
            }

            readPos += bytesRead;

            if (checkParameters && (readPos - reinterpret_cast<char *>(&inp)) > HEADER_SIZE)
            {
                checkParameters = false;

                float newFreq_hz = inp.freq_khz * 1000;
                int newSamplerate_smpls = inp.samplerate_ksmpls * 1000;
                if (newFreq_hz != freq_hz || newSamplerate_smpls != samplerate_smpls)
                {
                    mDataConverter.abort();
                    freq_hz = newFreq_hz;
                    samplerate_smpls = newSamplerate_smpls;
                    {
                        std::lock_guard<std::mutex> lock(mMutex);
                        mFreq_hz = newFreq_hz;
                        mSamplrate_smpls = newSamplerate_smpls;
                    }
                }

                processPos = &inp.data[0];
            }

            if (processPos)
            {
                size_t dataSize = (reinterpret_cast<int16_t *>(readPos) - processPos);
                if (dataSize)
                {
                    size_t readyBlocks = mDataConverter.convertBlock(processPos, dataSize, outPtrs, OUT_BLOCKS);
                    processPos += dataSize;
                    for (int i = 0; i < readyBlocks; i++)
                    {
                        PostProcess(mChannelId, outPtrs[i]->data, OUTPUT_BLOCK_SIZE, freq_hz, samplerate_smpls);
                    }
                }
            }
        }
    }
}