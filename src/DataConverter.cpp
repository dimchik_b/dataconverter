#include "DataConverter.h"
#include "PostProcess.h"
#include <stdint.h>

float DataConverter::convertSample(int16_t sample)
{
    float res = static_cast<float>(sample & 0x7FFF) / (1 << FIXED_POINT_POWER);
    return sample & 0x8000 ? -res : res;
}

int DataConverter::convertBlock(int16_t *input, size_t input_size, OutputBlock *output[], size_t output_size)
{

    if (mOut == nullptr)
    {
        mOut = new OutputBlock();
        mNextSample = 0;
    }

    int readyBlocks = 0;

    for (int i = 0; i < input_size; i++)
    {
        mOut->data[mNextSample] = convertSample(input[i]);
        mNextSample++;
        if (mNextSample == OUTPUT_BLOCK_SIZE)
        {
            if (output && output_size > readyBlocks)
            {
                output[readyBlocks] = mOut;
                readyBlocks++;
            }
            else
            {
                delete mOut;
            }

            mOut = new OutputBlock();
            mNextSample = 0;
        }
    }

    return readyBlocks;
}

void DataConverter::abort()
{
    delete mOut;
    mOut = nullptr;
    mNextSample = 0;
}