#ifndef POSTPROCESS_H
#define POSTPROCESS_H

/**
 * @brief Processes the data blocks
 * @param ch - the channel where the data are received
 * @param block - a pointer to the data block
 * @param size - a number of samples in the block. Should be 5120
 * @param freq_hz - a frequency of the channel in Hz
 * @param samplerate_smpls - a samplerate of the channel in samples per second
 **/
void PostProcess(int ch, float *block, int size, float freq_hz,
                 int samplrate_smpls);

#endif // POSTPROCESS_H