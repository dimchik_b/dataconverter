#ifndef CHANNEL_H
#define CHANNEL_H

#include "DataConverter.h"
#include <atomic>
#include <mutex>
#include <thread>

typedef int (*ReceiveHandler)(void *ptr, size_t size);

struct Channel
{
    Channel(ReceiveHandler handler, int channelId);
    virtual ~Channel();
    void StartCH();
    void StopCH();

private:
    void run();

    ReceiveHandler mHandler;
    int mChannelId;
    DataConverter mDataConverter;
    std::thread mRunThread;
    std::atomic_bool mActive = false;

    std::mutex mMutex;
    float mFreq_hz = 0;
    int mSamplrate_smpls = 0;
};

#endif // CHANNEL_H