#include "ConversionFacade.h"

void ConversionFacade::AddCH(int channelNo, ReceiveHandler h)
{
    mChannels[channelNo] = std::make_unique<Channel>(h, channelNo);
};

void ConversionFacade::RemoveCH(int channelNo)
{
    mChannels.erase(channelNo);
};

void ConversionFacade::StartCH(int channelNo)
{
    auto it = mChannels.find(channelNo);
    if (it != mChannels.end())
    {
        mChannels[channelNo]->StartCH();
    }
};

void ConversionFacade::StopCH(int channelNo)
{
    auto it = mChannels.find(channelNo);
    if (it != mChannels.end())
    {
        mChannels[channelNo]->StopCH();
    }
};

std::unique_ptr<Channel> ConversionFacade::GetCH(int channelNo)
{
    std::unique_ptr<Channel> channel;
    auto it = mChannels.find(channelNo);
    if (it != mChannels.end())
    {
        channel = std::move(it->second);
        mChannels.erase(it);
    }

    return channel;
};