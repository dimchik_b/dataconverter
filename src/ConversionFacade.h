#ifndef CONVERSION_FACADE_H
#define CONVERSION_FACADE_H

#include "Channel.h"
#include <map>
#include <memory>

struct ConversionFacade
{
    void AddCH(int channelNo, ReceiveHandler h);
    void RemoveCH(int channelNo);
    void StartCH(int channelNo);
    void StopCH(int channelNo);
    std::unique_ptr<Channel> GetCH(int channelNo);

private:
    std::map<int, std::unique_ptr<Channel>> mChannels;
};

#endif // CONVERSION_FACADE_H