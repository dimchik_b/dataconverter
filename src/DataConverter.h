#ifndef DATA_CONVERTER_H
#define DATA_CONVERTER_H

#include <stdint.h>

const size_t INPUT_BLOCK_SIZE = 2048;
const size_t OUTPUT_BLOCK_SIZE = 5120;
const uint8_t FIXED_POINT_POWER = 13;

struct InputBlock
{
    short int freq_khz;
    short int samplerate_ksmpls;
    short int data[INPUT_BLOCK_SIZE];
};

struct OutputBlock
{
    float data[OUTPUT_BLOCK_SIZE];
};

struct DataConverter
{
    /**
     * @brief converts an input data in the fixed point format to the floating point blocks
     * of OUTPUT_BLOCK_SIZE samples
     *
     * @param input - a pointer to the input data
     * @param input_size - a size of the input data
     * @param output - an array of the output block pointers
     * @param output_size - a size of the output data array
     *
     * @return a number of the ready output blocks
     *
     * @note The output data array must be allocated by caller. The function fills it
     * with pointers to output blocks. The caller is responsible for freeing them.
     */
    int convertBlock(int16_t *input, size_t input_size, OutputBlock *output[], size_t output_size);

    /**
     * @brief aborts creation of the next output block
     */
    void abort();

private:
    float convertSample(int16_t sample);

private:
    OutputBlock *mOut = nullptr;
    size_t mNextSample = 0;
};

#endif // DATA_CONVERTER_H